#include <gtest/gtest.h>
#include <iostream>
using namespace std;
int main(int argc, char **argv) {
    cout << "Test start!" << endl;
    testing::InitGoogleTest(&argc, argv);
    int result = RUN_ALL_TESTS();
    cout << "End test!" << endl;
    return result;
}